#include "hardware/pio.h"
#include "ymf262.pio.h"

#include <hardware/irq.h>
#include <hardware/structs/sio.h>
#include <hardware/uart.h>
#include <pico/multicore.h>
#include <string.h>
#include <tusb.h>

#include "OPL2.h"

#if !defined(MIN)
#define MIN(a, b) ((a > b) ? b : a)
#endif /* MIN */

#define UART_ID uart0
#define BAUD_RATE 115200
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY    UART_PARITY_NONE
#define UART_TX_PIN 0
#define UART_RX_PIN 1

#define OPL_RESET_PIN 16


static PIO pio;
static uint sm;

void writeAddress(PIO pio, uint sm, uint8_t address, uint8_t a1) {
    uint32_t value = address;
    value |= ((a1 ? 0b10 : 0b00) << 8);
    //printf("Address %03x\n", value);
    pio_sm_put_blocking(pio, sm, value);
}

void writeData(PIO pio, uint sm, uint8_t data) {
    uint32_t value = data;
    value |= 0b11 << 8;
    //printf("Data    %03x\n", value);
    pio_sm_put_blocking(pio, sm, value);
}

void writeRegister(PIO pio, uint sm, uint8_t a1, uint8_t reg, uint8_t data) {
    writeAddress(pio, sm, reg, a1);
    writeData(pio, sm, data);
}

static uint8_t buffer[3];
static int chars_rxed = 0;
static bool reset_opl = false;

void process_next_byte(uint8_t byte) {
        uint8_t packet_num = (byte & 0b11000000) >> 6;
        if (byte == 0xFF) {
            reset_opl = true;
        } else if (packet_num == chars_rxed && !reset_opl) {
            buffer[chars_rxed] = byte;
            chars_rxed++;
            if (chars_rxed >= 3) {
                chars_rxed = 0;
                uint8_t a1    = (buffer[0] & 0b00010000) >> 4;
                uint8_t addr  = (buffer[0] & 0b00001111) << 4 | (buffer[1] & 0b00111100) >> 2;
                uint8_t value = (buffer[1] & 0b00000011) << 6 | (buffer[2] & 0b00111111);
                writeRegister(pio, sm, a1, addr, value);
                //printf("a1: %02x addr: %02x data: %02x\n", a1, addr, value);
            }
        } else {
            // incomplete packet, reset to 0 and wait for next one
            chars_rxed = 0;
        }
}

// RX interrupt handler
void on_uart_rx() {
    while (uart_is_readable(UART_ID)) {
        uint8_t ch = uart_getc(UART_ID);
        process_next_byte(ch);
    }
}

static void cdc_task(void)
{
  uint8_t itf;

  for (itf = 0; itf < CFG_TUD_CDC; itf++)
  {
        if (tud_cdc_n_connected(itf)) {
            uint32_t available = tud_cdc_n_available(itf);
            if (available) {
                uint8_t buf[64];

                uint32_t count = tud_cdc_n_read(itf, buf, sizeof(buf));

                for (uint32_t i=0; i<count; i++) {
                    process_next_byte(buf[i]);
                }
            }
        }
    }
}

int main() {
    pio = pio0;

    gpio_init(PICO_DEFAULT_LED_PIN);
    gpio_init(OPL_RESET_PIN);
    gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
    gpio_set_dir(OPL_RESET_PIN, GPIO_OUT);
	//sleep_ms(5000);
    gpio_put(PICO_DEFAULT_LED_PIN, 1);

    uint offset = pio_add_program(pio, &ymf262_program);

    sm = pio_claim_unused_sm(pio, true);
    ymf262_program_init(pio, sm, offset, 2);

    gpio_put(OPL_RESET_PIN, 0);
    sleep_ms(100);
    gpio_put(OPL_RESET_PIN, 1);
    sleep_ms(100);

	tusb_init();

	while(1)
	{
        if (reset_opl) {
            gpio_put(OPL_RESET_PIN, 0);
            sleep_ms(10);
            gpio_put(OPL_RESET_PIN, 1);
            reset_opl = false;
        }
		tud_task();
		cdc_task();
	}

/*     uart_init(UART_ID, BAUD_RATE);
    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);
    uart_set_baudrate(UART_ID, BAUD_RATE);
    uart_set_hw_flow(UART_ID, false, false);
    uart_set_format(UART_ID, DATA_BITS, STOP_BITS, PARITY);
    uart_set_fifo_enabled(UART_ID, false);
    int UART_IRQ = UART_ID == uart0 ? UART0_IRQ : UART1_IRQ;
    irq_set_exclusive_handler(UART_IRQ, on_uart_rx);
    irq_set_enabled(UART_IRQ, true);
    uart_set_irq_enables(UART_ID, true, false);


    while (1) {
    } */

/*      OPL2 opl2(pio, sm);

	opl2.begin();

	// Setup channels 0, 1 and 2 to produce a bell sound.
    opl2.setTremolo   (0, CARRIER, true);
    opl2.setVibrato   (0, CARRIER, true);
    opl2.setMultiplier(0, CARRIER, 0x04);
    opl2.setAttack    (0, CARRIER, 0x0A);
    opl2.setDecay     (0, CARRIER, 0x04);
    opl2.setSustain   (0, CARRIER, 0x0F);
    opl2.setRelease   (0, CARRIER, 0x0F);
    opl2.setVolume    (0, CARRIER, 0x00);

    while(true) {
        // Play notes C-3 through B-4 on alternating channels.
        for (uint8_t i = 0; i < 24; i ++) {
            uint8_t octave = 3 + (i / 12);
            uint8_t note = i % 12;
            opl2.playNote(0, octave, note);
            sleep_ms(500);
        }
    } */
}