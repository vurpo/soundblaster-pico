# Serial protocol documentation:

The protocol works on packets. Each packet is 3 bytes long, and writes one value to a register on the YMF262.

The most significant two bits of every byte determines the byte number (you must send packet 0, then 1, then 2 in order to send a full packet). If the byte number is out of order, the packet is discarded and processing is started again from 0.

If the byte 0xFF is received at any point, the OPL is reset.

Each byte contains the information packed in the following way:

Byte 0: PPxBAAAA

Byte 1: PPAAAADD

Byte 2: PPDDDDDD

where P is the byte number, B is the A1 line (bank), A is the address, and D is the data (all in order of highest to lowest bit). After byte 0, 1, and then 2 is successfully received, the given value is immediately written to the given address on the YMF262.
