#pragma once

#include "pico/stdlib.h"
#include "hardware/pio.h"

// Generic OPL2 definitions.
#define OPL2_NUM_CHANNELS 9
#define CHANNELS_PER_BANK 9

// Operator definitions.
#define OPERATOR1 0
#define OPERATOR2 1
#define MODULATOR 0
#define CARRIER   1

// Synthesis type definitions.
#define SYNTH_MODE_FM 0
#define SYNTH_MODE_AM 1

// Drum sounds.
#define DRUM_BASS   0
#define DRUM_SNARE  1
#define DRUM_TOM    2
#define DRUM_CYMBAL 3
#define DRUM_HI_HAT 4

// Drum sound bit masks.
#define DRUM_BITS_BASS   0x10
#define DRUM_BITS_SNARE  0x08
#define DRUM_BITS_TOM    0x04
#define DRUM_BITS_CYMBAL 0x02
#define DRUM_BITS_HI_HAT 0x01

// Note to frequency mapping.
#define NOTE_C   0
#define NOTE_CS  1
#define NOTE_D   2
#define NOTE_DS  3
#define NOTE_E   4
#define NOTE_F   5
#define NOTE_FS  6
#define NOTE_G   7
#define NOTE_GS  8
#define NOTE_A   9
#define NOTE_AS 10
#define NOTE_B  11

// Tune specific declarations.
#define NUM_OCTAVES      7
#define NUM_NOTES       12
#define NUM_DRUM_SOUNDS  5

// Instrument data sources (Arduino only).
#if BOARD_TYPE == OPL2_BOARD_TYPE_ARDUINO
	#define INSTRUMENT_DATA_PROGMEM true
	#define INSTRUMENT_DATA_SRAM false
#endif

struct Operator {
	bool hasTremolo;
	bool hasVibrato;
	bool hasSustain;
	bool hasEnvelopeScaling;
	uint8_t frequencyMultiplier;
	uint8_t keyScaleLevel;
	uint8_t outputLevel;
	uint8_t attack;
	uint8_t decay;
	uint8_t sustain;
	uint8_t release;
	uint8_t waveForm;
};


struct Instrument {
	Operator operators[2];
	uint8_t feedback;
	bool isAdditiveSynth;
	uint8_t transpose;
};


class OPL2 {
	public:
		OPL2(PIO pio, uint sm);
		virtual void begin();
		virtual void reset();
		virtual void createShadowRegisters();
		void init();

		virtual uint8_t getChipRegister(short reg);
		virtual uint8_t getChannelRegister(uint8_t baseRegister, uint8_t channel);
		virtual uint8_t getOperatorRegister(uint8_t baseRegister, uint8_t channel, uint8_t op);
		virtual uint8_t getRegisterOffset(uint8_t channel, uint8_t operatorNum);
		virtual void setChipRegister(short reg, uint8_t value);
		virtual void setChannelRegister(uint8_t baseRegister, uint8_t channel, uint8_t value);
		virtual void setOperatorRegister(uint8_t baseRegister, uint8_t channel, uint8_t op, uint8_t value);
		virtual uint8_t getChipRegisterOffset(short reg);
		virtual uint8_t getChannelRegisterOffset(uint8_t baseRegister, uint8_t channel);
		virtual short getOperatorRegisterOffset(uint8_t baseRegister, uint8_t channel, uint8_t operatorNum);
		virtual void write(uint8_t reg, uint8_t data);

		virtual uint8_t getNumChannels();

		float getFrequency(uint8_t channel);
		void setFrequency(uint8_t channel, float frequency);
		uint8_t getFrequencyBlock(float frequency);
		short getFrequencyFNumber(uint8_t channel, float frequency);
		short getNoteFNumber(uint8_t note);
		float getFrequencyStep(uint8_t channel);
		void playNote(uint8_t channel, uint8_t octave, uint8_t note);
		void playDrum(uint8_t drum, uint8_t octave, uint8_t note);

		Instrument createInstrument();
		Instrument loadInstrument(const unsigned char *instrument);
		Instrument getInstrument(uint8_t channel);
		void setInstrument(uint8_t channel, Instrument instrument, float volume = 1.0);
		void setDrumInstrument(Instrument instrument, uint8_t drumType, float volume = 1.0);

		virtual bool getWaveFormSelect();
		bool getTremolo(uint8_t channel, uint8_t operatorNum);
		bool getVibrato(uint8_t channel, uint8_t operatorNum);
		bool getMaintainSustain(uint8_t channel, uint8_t operatorNum);
		bool getEnvelopeScaling(uint8_t channel, uint8_t operatorNum);
		uint8_t getMultiplier(uint8_t channel, uint8_t operatorNum);
		uint8_t getScalingLevel(uint8_t channel, uint8_t operatorNum);
		uint8_t getVolume(uint8_t channel, uint8_t operatorNum);
		uint8_t getChannelVolume(uint8_t channel);
		uint8_t getAttack(uint8_t channel, uint8_t operatorNum);
		uint8_t getDecay(uint8_t channel, uint8_t operatorNum);
		uint8_t getSustain(uint8_t channel, uint8_t operatorNum);
		uint8_t getRelease(uint8_t channel, uint8_t operatorNum);
		short getFNumber(uint8_t channel);
		uint8_t getBlock(uint8_t channel);
		bool getNoteSelect();
		bool getKeyOn(uint8_t channel);
		uint8_t getFeedback(uint8_t channel);
		uint8_t getSynthMode(uint8_t channel);
		bool getDeepTremolo();
		bool getDeepVibrato();
		bool getPercussion();
		uint8_t getDrums();
		uint8_t getWaveForm(uint8_t channel, uint8_t operatorNum);

		virtual void setWaveFormSelect(bool enable);
		void setTremolo(uint8_t channel, uint8_t operatorNum, bool enable);
		void setVibrato(uint8_t channel, uint8_t operatorNum, bool enable);
		void setMaintainSustain(uint8_t channel, uint8_t operatorNum, bool enable);
		void setEnvelopeScaling(uint8_t channel, uint8_t operatorNum, bool enable);
		void setMultiplier(uint8_t channel, uint8_t operatorNum, uint8_t multiplier);
		void setScalingLevel(uint8_t channel, uint8_t operatorNum, uint8_t scaling);
		void setVolume(uint8_t channel, uint8_t operatorNum, uint8_t volume);
		void setChannelVolume(uint8_t channel, uint8_t volume);
		void setAttack(uint8_t channel, uint8_t operatorNum, uint8_t attack);
		void setDecay(uint8_t channel, uint8_t operatorNum, uint8_t decay);
		void setSustain(uint8_t channel, uint8_t operatorNum, uint8_t sustain);
		void setRelease(uint8_t channel, uint8_t operatorNum, uint8_t release);
		void setFNumber(uint8_t channel, short fNumber);
		void setBlock(uint8_t channel, uint8_t block);
		void setNoteSelect(bool enable);
		void setKeyOn(uint8_t channel, bool keyOn);
		void setFeedback(uint8_t channel, uint8_t feedback);
		void setSynthMode(uint8_t channel, uint8_t synthMode);
		void setDeepTremolo(bool enable);
		void setDeepVibrato(bool enable);
		void setPercussion(bool enable);
		void setDrums(uint8_t drums);
		void setDrums(bool bass, bool snare, bool tom, bool cymbal, bool hihat);
		void setWaveForm(uint8_t channel, uint8_t operatorNum, uint8_t waveForm);

	protected:
		template <typename T>
		T clampValue(T value, T min, T max);
		
		PIO pio;
		uint sm;

		uint8_t* chipRegisters;
		uint8_t* channelRegisters;
		uint8_t* operatorRegisters;

		uint8_t numChannels = OPL2_NUM_CHANNELS;

		const float fIntervals[8] = {
			0.048, 0.095, 0.190, 0.379, 0.759, 1.517, 3.034, 6.069
		};
		const unsigned int noteFNumbers[12] = {
			0x156, 0x16B, 0x181, 0x198, 0x1B0, 0x1CA,
			0x1E5, 0x202, 0x220, 0x241, 0x263, 0x287
		};
		const float blockFrequencies[8] = {
				48.503,   97.006,  194.013,  388.026,
			776.053, 1552.107, 3104.215, 6208.431
		};
		const uint8_t registerOffsets[2][9] = {  
			{ 0x00, 0x01, 0x02, 0x08, 0x09, 0x0A, 0x10, 0x11, 0x12 } ,   /*  initializers for operator 1 */
			{ 0x03, 0x04, 0x05, 0x0B, 0x0C, 0x0D, 0x13, 0x14, 0x15 }     /*  initializers for operator 2 */
		};
		const uint8_t drumRegisterOffsets[2][5] = {
			{ 0x10, 0xFF, 0x12, 0xFF, 0x11 },
			{ 0x13, 0x14, 0xFF, 0x15, 0xFF }
		};
		const uint8_t drumChannels[5] = {
			6, 7, 8, 8, 7
		};
		const uint8_t drumBits[5] = {
			DRUM_BITS_BASS, DRUM_BITS_SNARE, DRUM_BITS_TOM, DRUM_BITS_CYMBAL, DRUM_BITS_HI_HAT
		};
};