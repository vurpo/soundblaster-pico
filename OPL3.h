#pragma once
#include "OPL2.h"

#define OPL3_NUM_2OP_CHANNELS 18
#define OPL3_NUM_4OP_CHANNELS 6
#define CHANNELS_PER_BANK 9

#define SYNTH_MODE_FM_FM 0
#define SYNTH_MODE_FM_AM 1
#define SYNTH_MODE_AM_FM 2
#define SYNTH_MODE_AM_AM 3


struct Instrument4OP {
	Instrument subInstrument[2];		// Definition of the 2 sub instruments for each channel.
										// Transpose of the instrument is found in transpose of sub instrument 0.
};


class OPL3: public OPL2 {
	public:
		OPL3(PIO pio, uint sm);
		virtual void begin();
		virtual void reset();
		virtual void createShadowRegisters();

		virtual void setChipRegister(short baseRegister, uint8_t value);
		virtual void setChannelRegister(uint8_t baseRegister, uint8_t channel, uint8_t value);
		virtual void setOperatorRegister(uint8_t baseRegister, uint8_t channel, uint8_t operatorNum, uint8_t value);
		virtual uint8_t getChipRegisterOffset(short reg);
		virtual void write(uint8_t bank, uint8_t reg, uint8_t value);

		virtual uint8_t getNumChannels();
		virtual uint8_t getNum4OPChannels();
		virtual uint8_t get4OPControlChannel(uint8_t channel4OP, uint8_t index2OP = 0);

		Instrument4OP createInstrument4OP();
		Instrument4OP loadInstrument4OP(const unsigned char *instrument);
		Instrument4OP getInstrument4OP(uint8_t channel4OP);
		void setInstrument4OP(uint8_t channel4OP, Instrument4OP instrument, float volume = 1.0);

		virtual bool getWaveFormSelect();
		virtual void setWaveFormSelect(bool enable = false);

		virtual bool isOPL3Enabled();
		virtual void setOPL3Enabled(bool enable);
		virtual bool is4OPChannelEnabled(uint8_t channel4OP);
		virtual void set4OPChannelEnabled(uint8_t channel4OP, bool enable);
		virtual void setAll4OPChannelsEnabled(bool enable);

		bool isPannedLeft (uint8_t channel);
		bool isPannedRight(uint8_t channel);
		void setPanning(uint8_t channel, bool left, bool right);
		uint8_t get4OPSynthMode(uint8_t channel4OP);
		void set4OPSynthMode(uint8_t channel4OP, uint8_t synthMode);
		uint8_t get4OPChannelVolume(uint8_t channel4OP);
		void set4OPChannelVolume(uint8_t channel4OP, uint8_t volume);


	protected:

		uint8_t numChannels = OPL3_NUM_2OP_CHANNELS;
		uint8_t num4OPChannels = OPL3_NUM_4OP_CHANNELS;

		uint8_t channelPairs4OP[6][2] = {
			{ 0,  3 }, {  1,  4 }, {  2,  5 },
			{ 9, 12 }, { 10, 13 }, { 11, 14 }
		};
};
